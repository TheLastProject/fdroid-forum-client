package at.h4x.fdroidforumclient;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.RawRes;
import android.webkit.ValueCallback;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

class GenericWebViewClient extends WebViewClient {

    private MainActivity mainActivity;
    //private CookieManager cookieManager;

    public GenericWebViewClient(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

        // check whether url belongs to this app
        for (String webAppUrl : mainActivity.getResources().getStringArray(R.array.webapp_urls)) {
            if (request.getUrl().toString().matches(webAppUrl)) {
                return false;
            }
        }

        // url not a part of this app, tell android start it in a web browser
        Intent intent = new Intent(Intent.ACTION_VIEW, request.getUrl());
        intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        mainActivity.startActivity(intent);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        String augemntScript = "(" + rawToUtf8String(mainActivity, R.raw.augment_script) + ")();";
        //Log.i("###", "augment script for " + url + ":\n" + augemntScript);
        view.evaluateJavascript(augemntScript, new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
            }
        });
    }

    String rawToUtf8String(Context context, @RawRes int rawId) {
        BufferedReader in = null;
        try {
            StringBuilder sb = new StringBuilder();
            in  = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(rawId), "UTF-8"));
            for (String line; (line = in.readLine()) != null;) {
                sb.append(line);
                sb.append("\n");
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e) {
        } catch (IOException e) {
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }
        return "";
    }
}
