# F-Droid Forum Client

A simple wrapper for F-Droid Forum (https://forum.f-droid.org)

This app only access anything other the F-Droid Forum. You can read the privacy
policy of F-Droid Forum here: https://forum.f-droid.org/privacy

<img alt="logo" src="https://codeberg.org/uniqx/fdroid-forum-client/raw/branch/main/metadata/en-US/images/icon.png" />

## Screenshots

<a href="https://codeberg.org/uniqx/fdroid-forum-client/raw/branch/main/metadata/en-US/images/phoneScreenshots/Screenshot1.png">
  <img alt="Screenshot: browsing posts" src="https://codeberg.org/uniqx/fdroid-forum-client/raw/branch/main/metadata/en-US/images/phoneScreenshots/Screenshot1.png" width="150" />
</a>

<a href="https://codeberg.org/uniqx/fdroid-forum-client/raw/branch/main/metadata/en-US/images/phoneScreenshots/Screenshot2.png">
  <img alt="Screenshot: reading a post" src="https://codeberg.org/uniqx/fdroid-forum-client/raw/branch/main/metadata/en-US/images/phoneScreenshots/Screenshot2.png" width="150" />
</a>

<a href="https://codeberg.org/uniqx/fdroid-forum-client/raw/branch/main/metadata/en-US/images/phoneScreenshots/Screenshot3.png">
  <img alt="Screenshot: app menu" src="https://codeberg.org/uniqx/fdroid-forum-client/raw/branch/main/metadata/en-US/images/phoneScreenshots/Screenshot3.png" width="150" />
</a>
